---
author: "Esben Kran"
date: 2020-06-27
title: Brain-computer interfacing and the future of humanity
next: /next-blog-post-name-with-hyphens
prev: /sample-blog-post
categories: ["neuroscience", "cognitive science", "altruism"]
tags: ["example", "brain-computer interfacing"]
bookHidden: true
---

# Brain-computer interfacing

## Past

### Origins

## Present

### Neuralink

### Kernel

## Future
