---
author: "Author"
date: 2020-05-05
title: This is a cool blog post
next: /next-blog-post-name-with-hyphens
prev: /previous-blog-post-name
categories: ["contribute"]
tags: ["example"]
bookHidden: true
---

The first part of the blog post becomes the summary / description on the blog page.

# This is my title

### [Read instructions here](/content/docs/contribute/writing-on-the-cogsite.md)

And this is my text.
