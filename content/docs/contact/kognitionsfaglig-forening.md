---
weight: 10
# bookFlatSection: true
title: "Kognitionsfaglig Forening"
---

# Kognitionsfaglig Forening

## Broca's Bodega

Email: cognitivescienceau@gmail.com<br/>
Blog: https://mochawithbroca.com<br/>
About: This is a university friday bar at Aarhus University for Cognitive Science students and friends.<br/>

## Gazzaniga's Gang
Email: snuggle.hogwarts@gmail.com<br/>
About: The Gazzaniga's Gang, is an event group dedicated to making our collective CogSci Family even more close-knit<br/>
