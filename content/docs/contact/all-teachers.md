---
weight: 10
# bookFlatSection: true
title: "All teachers"
---

# Bachelor
{{< details title="1st Semester" open=false >}}
### Kristian Tylén (Teacher in Cognition & Communication)
[Mail](kristian@cc.au.dk)
### Sebastian Scott Engen (Student Instructor in Cognition & Communication)
[Mail](Snuggle.hogwarts@gmail.com)
### Fabio Trecca (Teacher in Methods 1)
[Mail](fabio@cc.au.dk)
### Jonathan Hvithamar Rystrøm (Student Instructor in Methods 1)
[Mail](201907021@post.au.dk)
### Daina Crafa (Teacher in Introduction to Cognitive Science)
[Mail](daina.crafa@cas.au.dk)
### Anders Weile Larsen (Student Instructor in Introduction to Cognitive Science)
[Mail](aweile@icloud.com)
{{< /details >}}

{{< details title="3rd Semester" open=false >}}
### Byurakn Ishkhanyan (Teacher in Methods 3)
[Mail](byurakn@cc.au.dk)
### Victor Poulsen (Student Instructor in Methods 3)
[Mail](201707639@post.au.dk)
### Daina Crafa (Teacher in Mind & Conciousness)
[Mail](daina.crafa@cas.au.dk)
{{< /details >}}

# Master 
{{< details title="1st Semester" open=false >}}
### Chris Mathys (Teacher in Deciosionmaking)
[Mail](chmathys@cas.au.dk)
### Rebekah Brita Baglini (Teacher in Natural Language Processing)
[Mail](rbkh@cc.au.dk)
### Mads Jensen (Teacher in Advanced Cognitive Neuroscience)
[Mail](mads@cas.au.dk)
{{< /details >}}

# Aarhus University
{{< details title="Cognitive Science Staff" open=false >}}
### Joshua Charles Skewes (Head of Department at Linguistics, Cognitive Science and Semiotics)
[Mail](filjcs@cc.au.dk)
### Blanka Sara Palfi (Instructor of Study Techniques)
[Mail](blankap@cc.au.dk)
### Sofie Nødskov Ditmer Sørensen (Student Instructor)
[Mail](201805308@post.au.dk)
{{< /details >}}






```
