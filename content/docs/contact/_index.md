---
weight: 13
bookCollapseSection: true
# bookFlatSection: true
title: "Contact"
---

# Contact page

You are always welcome to contact the people who are currently working on the wbsite. Help is always welcome! 

Email: cogscidk@gmail.com

Current contributors:

- Arnault-Quentin Vermillet, organization
- Peter Thestrup Waade, content
- Esben Kran, tech and content
- Sebastian Scott Engen, content
- Malte Lau Pedersen, tech
- Millie Søndergaard, career
- Ludvig Olsen, external supervisor
- Blanka Pálfi, content and mental health
