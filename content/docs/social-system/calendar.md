---
weight: 10
# bookFlatSection: true
title: "Academic Event Calendar"
---

# Neural Network calendar

<iframe src="https://calendar.google.com/calendar/embed?src=waade.net_g1s9lthnsk8q4rp6b71ha53ti0%40group.calendar.google.com&ctz=Europe%2FCopenhagen" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>