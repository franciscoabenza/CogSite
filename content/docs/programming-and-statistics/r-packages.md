---
weight: 1
title: "R packages"
bookToC: false
---

# Useful R packages

{{< details title="Basic utilities" open=false >}}

| Package                                                   | Description                                      | By             |
| :-------------------------------------------------------- | :----------------------------------------------- | :------------- |
| [`dplyr`](https://github.com/tidyverse/dplyr)             | A grammar of data manipulation                   | tidyverse      |
| [`tidyr`](https://github.com/tidyverse/tidyr)             | Tidy data with spread and gather functions       | tidyverse      |
| [`purrr`](https://github.com/tidyverse/purrr)             | A functional programming toolkit                 | tidyverse      |
| [`plyr`](https://github.com/hadley/plyr)                  | `llply()` and `ldply()` as for-loop replacements | Hadley Wickham |
| [`tibble`](https://github.com/tidyverse/tibble/)          | Modern reimagining of the `data.frame`           | tidyverse      |
| [`data.table`](https://github.com/Rdatatable/data.table)  | Alternative data frame. Quicker for big datasets | Tyson Barrett  |
| [`recipes`](https://github.com/tidymodels/recipes)        | Preprocessing pipelines                          | tidyverse |
| [`groupdata2`](https://github.com/LudvigOlsen/groupdata2) | Create groups, folds and partitions.             | Ludvig Olsen   |

{{< /details >}}

{{< details title="Text manipulation" open=false >}}

| Package                                              | Description                   | By                                                                          |
| :--------------------------------------------------- | :---------------------------- | :-------------------------------------------------------------------------- |
| [`stringr`](https://github.com/tidyverse/stringr)    | String manipulation           | tidyverse                                                                   |
| [`tidytext`](https://github.com/juliasilge/tidytext) | Tidy text analysis            | Julia Silge, David Robinson                                                 |
| [`quanteda`](https://github.com/quanteda/quanteda)   | Quantitative analysis of text | quanteda                                                                    |
| [`spacyr`](https://github.com/quanteda/spacyr)       | spaCy API for R               | quanteda                                                                    |
| [`sentida`](https://github.com/Guscode/Sentida)      | Sentiment analysis            | Lars Kjartan Bacher Svendsen, Jacob Aarup Dalsgaard, Gustav Aarup Lauridsen |

{{< /details >}}

{{< details title="Modeling" open=false >}}

| Package                                                   | Description                                | By            |
| :-------------------------------------------------------- | :----------------------------------------- | :------------ |
| [`lme4`](https://github.com/lme4/lme4)                    | Linear mixed-effects models                | Douglas Bates, et al. |
| [`brms`](https://github.com/paul-buerkner/brms)           | Bayesian modeling                          | Paul-Christian Bürkner |
| [`cvms`](https://github.com/LudvigOlsen/cvms)             | Evaluation and cross-validation of models  | Ludvig Olsen  |
| [`parsnip`](https://github.com/tidymodels/parsnip)        | Tidy, unified interface to model functions | tidymodels    |
| [`superlearner`](https://github.com/ecpolley/SuperLearner)| Ensemble methods                           | Eric Polley, et al. |
| [`keras`](https://keras.rstudio.com/)                     | Deep learning framework                    | Daniel Falbel, et al. |
| [`tensorflow`](https://tensorflow.rstudio.com/)           | Deep learning framework                    | Daniel Falbel, et al. |
| [`mlr3`](https://github.com/mlr-org/mlr3/)                | Object-oriented machine learning framework | Michel Lang, et al. |
| [`e1071`](https://cran.r-project.org/package=e1071)       | Different model functions                  | David Meyer, et al. |
| [`performance`](https://github.com/easystats/performance) | Evaluating performance of models           | easystats     |
| [`yardstick`](https://github.com/tidymodels/yardstick/)   | Evaluation metrics                         | tidymodels    |
| [`insight`](https://github.com/easystats/insight)         | Extract model attributes                   | easystats     |
| [`MuMIn`](https://cran.r-project.org/package=MuMIn)       | Model object metrics                       | Kamil Bartoń  |

{{< /details >}}

{{< details title="Visualization" open=false >}}

| Package                                               | Description              | By                  |
| :---------------------------------------------------- | :----------------------- | :------------------ |
| [`ggplot2`](https://github.com/tidyverse/ggplot2)     | Make plots               | tidyverse           |
| [`patchwork`](https://github.com/thomasp85/patchwork) | Combine separate ggplots | Thomas Lin Pedersen |
| [`see`](https://github.com/easystats/see)             | Visualisation toolbox    | easystats           |
| [`ggimage`](https://github.com/GuangchuangYu/ggimage) | Add images to ggplots    | Guangchuang YU      |

{{< /details >}}

{{< details title="R development" open=false >}}

| Package                                           | Description                          | By             |
| :------------------------------------------------ | :----------------------------------- | :------------- |
| [`usethis`](https://github.com/r-lib/usethis)     | Automate package and project setup tasks. | Hadley Wickham, et al. |
| [`testthat`](https://testthat.r-lib.org/)         | Framework for unit-testing functions | Hadley Wickham |
| [`xpectr`](https://github.com/LudvigOlsen/xpectr) | Generate `testthat` tests            | Ludvig Olsen   |
| [`checkmate`](https://github.com/mllg/checkmate)  | Fast input checks in functions       | Michel Lang    |

{{< /details >}}

