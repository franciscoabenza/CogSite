---
weight: 6
bookCollapseSection: true
# bookFlatSection: true
bookHidden: false
title: "Programming and statistics"
---

# Programming

These pages are about programming in data science and cognitive science.

## Index

- [R cheatsheet for actions](r-cheat-site.md)
- [R packages](r-packages.md)
