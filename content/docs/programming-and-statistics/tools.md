---
weight: 10
title: "Tools"
---
# Programs

{{< details title="Rstudio" open=false >}}
RStudio provides free and open source tools for R

https://rstudio.com/
{{< /details >}}

{{< details title="JASP" open=false >}}
The JASP interface allows you to conduct statistical analyses in seconds, and
without having to learn programming or risking a programming mistake.

https://jasp-stats.org/
{{< /details >}}

{{< details title="Psychopy" open=false >}}
Python builder (Perfect for scientific experiments)

https://www.psychopy.org/index.html
{{< /details >}}

{{< details title="Pavlovia" open=false >}}
Pavlovia (Psychopy In The Sky)

https://pavlovia.org/
{{< /details >}}

# Tools

{{< details title="Github & Gitlab" open=false >}}
Software development version control

- http://github.com/
- http://gitlab.com/
{{< /details >}}

# Plotting guides

{{< details title="ggplot2" open=false >}}
A Tutorial for Beautiful Plotting in R

https://cedricscherer.netlify.app/2019/08/05/a-ggplot2-tutorial-for-beautiful-plotting-in-r/
{{< /details >}}

{{< details title="STHDA" open=false >}}
Guides for 'Statistical tools for high-throughput data analysis'

http://www.sthda.com/english/articles/24-ggpubr-publication-ready-plots/
{{< /details >}}
