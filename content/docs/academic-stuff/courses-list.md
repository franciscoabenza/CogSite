---
weight: 8
# bookFlatSection: true
title: "Courses list"
---

# List of courses

ACADEMIC REGULATIONS FOR BACHELOR'S DEGREE PROGRAMME IN COGNITIVE SCIENCE (2015)
Version: Prepared by: Assigned body of external co-examiners:
2015 Board of Studies, School of Communication and Culture The body of external co-examiners for the Linguistic and Cognitive Subjects
Effective date: Approved by: The degree programme entitles the graduate to the title:
01-09-2015 Dean of Arts Bachelor (BSc) i kognitionsvidenskab
Bachelor of Science (BSc) in Cognitive Science
Prescribed period of study:
180 ECTS
Contents
About the degree programme
Degree programme structure
General rules
Changes to the academic regulations

1. About the degree programme
   1.1 The academic direction and primary subject areas of the programme
   The aims of the Bachelor’s degree programme in Cognitive Science are to provide the student with insight into cognitive processes such as perception and behaviour, decision-making, communication, social dynamics and consciousness with a particular focus on the latest developments within experimental and analytical methods. All the subjects focus on actual experimental projects and on the use of cognitive theory for solving problems in ‘the real world’, which will provide the student with skills that can be used in analysing and investigating a wide range of questions pertaining to human thought and behaviour.

This is a cross-disciplinary degree programme that combines in-dept academic specialisation with cross-disciplinary width. Cognitive science has developed into an independent line of research, but it draws on knowledge and procedures from other fields such as psychology, computer science, linguistics, philosophy and anthropology.

The degree programme is conducted in English. A supplementary subject (45 ECTS) is included in the Bachelor’s degree programme.

1.2 Learning outcomes
The Bachelor’s degree programme in Cognitive Science provides students with the following knowledge, skills and competences:

Knowledge and understanding:

A Bachelor of Cognitive Science:

has insight into basic cognitive processes associated with perception, actions, decision-making, communication, emotions, social dynamics, the mind and consciousness – and the way in which these all play out in the real world,
is familiar with both the theoretical and experimental perspectives on cognition coming from neuroscience, linguistics, psychology, philosophy and elsewhere
has in-depth knowledge of experimental methods that can be used to study human cognition both in and outside the lab,
has in-depth knowledge of methods for the analysis of empirical data concerning human cognition,
Skills:

A Bachelor of Cognitive Science is able to:

reflect critically at an academic level on questions related to human behaviour, experience and interaction,
search for, critically evaluate and communicate information found in all available sources on subjects related to cognitive science,
choose between several theoretical and empirical approaches to a given problem or assignment and combine several approaches when necessary,
design, programme, execute and analyse various types of experiments and studies concerning human behaviour, experience and interaction.
Competences:

A Bachelor of Cognitive Science is able to:

participate independently in cross-disciplinary collaborations concerning human behaviour, experience and interaction,
contribute critically to the collection of evidence-based experience concerning complex issues involving human behaviour, experience and interaction,
identify his/her own need for further knowledge acquisition and structure his/her own further learning,
synthesise and communicate the acquired knowledge in English.

1.3 This programme grants access to
Bachelor's degree programme in Cognitive Science qualifies graduates for The Master's degree programme in Cognitive Science at Aarhus University in accordance with the current rules in the field.

It is possible to qualify for admission to other Master’s degree programmes.

1.4 Transitional regulations
This degree programme has not been offered previously.

1.5 Time frame for the completion of the degree programme
The bachelor's degree programme must be completed within four years after enrollment.

Transitional regulations for students enrolled from 2 September 2013 to 31 August 2015:The bachelor's degree programme must be completed no later than 31 August 2019.

Transitional regulations for students enrolled 1 September 2013 or earlier:The bachelor's degree programme must be completed no later than 31 August 2018.

2. Degree programme structure
   2.1 Rules of the degree programme
   In addition to the general rules at the back of the academic regulation the following applies:

2.1.1 Lecture participation
Lecture participation can be used either as a prerequisite for taking an exam, or as a form of exam. Lecture participation means:
Regular, active and satisfactory participation in the teaching. Regular means participation in at least 75% of the teaching activities offered. At the start of the semester the teacher will announce in writing (on Blackboard) the teaching activities in which students are expected to participate, as well as the requirements regarding what will be construed as active and satisfactory participation.

2.1.2 Language of exam and teaching
The language of the exam is the same as the language of the teaching (normally English). The language of the teaching will be stated in the course catalogue. By arrangement with their teacher, students may take exams in Danish, English, Norwegian or Swedish in all the courses on the programme, unless one of the objectives of the exam is to demonstrate the students- skills in a foreign language.

2.1.3 Syllabus
The syllabus can be reused for up to three semesters: the semester in which the course is taught, and the two following semesters.
If an exam needs to be conducted in accordance with an old syllabus, the students are responsible for pointing this out.
The syllabus rules can be found on the study portal.

2.1.4 Tables, graphs and figures in written exams
Tables, graphs and figures correspond to 800 characters each (regardless of their size) if they are produced by the students. Tables, graphs and figures taken from other sources correspond to 0 characters.

2.1.5 First year exam for students enrolled before 1 september 2016
In the academic regulations for each Bachelor's degree programme, the university stipulates which exams the students must participate in before the end of the first year of their studies (the first-year exam). Students must pass the exams included in the first-year exam before the end of their second academic year in order to continue enrolment in their degree programme, cf. the Ministerial Order on University Examinations and Grading (the Examination Order) https://www.retsinformation.dk. For students enrolled before 1 September 2016, the examinations included in the first-year exam are: Experimental methods 1, Introduction to Cognitive Science and Cognition and communication.

Bachelor Cognitive Science

STUDYDIAGRAM
BACHELOR'S DEGREE PROGRAMME IN COGNITIVE SCIENCE
COMPULSORY
OPTIONAL
ELECTIVE
AUXILIARY

SEMESTER 1
Experimental methods 1
10 ECTS
Introduction to cognitive science
10 ECTS
Cognition and communication
10 ECTS
SEMESTER 2
Experimental methods 2
10 ECTS
Introduction to Cognitive Neuroscience
10 ECTS
Studium Generale
10 ECTS
SEMESTER 3
Experimental methods 3
10 ECTS
Mind and consciousness
10 ECTS
Humanities and International electives (HUM- & IV-fag)
10 ECTS
SEMESTER 4
Computational modeling for cognitive science
10 ECTS
Models of perception and action
10 ECTS
Social and cultural dynamics in cognition
10 ECTS
SEMESTER 5
Bachelor's project
15 ECTS
Elective subject
15 ECTS
SEMESTER 6
Elective subject
30 ECTS
Comment on study chart: The commencement of studies exam:
The commencement of studies exam is affiliated with the course Cognition and communication.

Experimental methods 1
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- Fundamental principles of experimental research as well as basic statistical concepts, assumptions and tests, including descriptive statistics and simple General Linear Model statistical tests, for example correlation, t-tests, ANOVAs etc.

Skills:
After completing the course, students will be able to:

- Design and develop simple experimental studies
- Plan and conduct simple experiments on behaviour
- Identify important assumptions for basic statistical tests
- Analyse data using descriptive and basic GLM statistics

Competences:
After completing the course, students will be able to:

- Critically assess and discuss published experimental research with a in terms of the methods and analyses that have been used
- Reflect on and account for relevant experimental designs suitable for the study of a given question
- Explain what a GLM analysis is and when it is applied
  See courses
  Experimental methods 1 10 ECTS Semester 1 Passed /failed
  1st year exam
  Exam details See courses

Introduction to Cognitive Science
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- Basic cognitive functions, such as perception, action, attention, memory, language, emotions and decision-making
- The relation between various cognitive functions
- Significant theoretical paradigms within cognitive science and their implications for empirical research
- Participation in cognitive experiments

Skills:
After completing the course, students will be able to:

- Describe the overall structure of the cognitive system
- Compare and discuss the most important paradigms within cognitive science, including computational, embodiment and dynamic system-theoretical approaches
- Compare and discuss the most important methodological approaches within cognitive science, including computational modelling, behavioural experiments and cognitive neuroscience
- Form simple experimental hypotheses related to cognition

Competences:
After completing the course, students will be able to:

- Acquire more complex theories and practice within cognitive science
- Identify key issues in cross-disciplinary research pertaining to human behaviour and cognition
  See courses
  Introduction to cognitive science 10 ECTS Semester 1 7-point grading scale
  1st year exam
  Exam details See courses

Cognition and communication
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- Similarities and differences between human language and animal communication systems
- The most important cognitive and social mechanisms associated with language acquisition, language development and language evolution
- Individual and social cognitive processes related to human communicative behaviour and communication problems associated with aphasia and mental disorders, for instance
- How meaning is structured in various forms of communicative modalities and media, for example conversation, written language, gestures and various types of mediated interaction

Skills:
After completing the course, students will be able to:

- Compare and discuss various perspectives on the relation between cognition and language
- Analyse communicative exchanges with regard to their semantic, structural and cognitive organisation
- Design and carry out studies of communicative behaviour

Competences:
After completing the course, students will be able to:

- Critically reflect on and discuss theoretical and empirical approaches to key topics within the field of communication and cognition
- Justify the choice between relevant methods and analyses used for specific research questions within the field of language and cognition
  See courses
  Cognition and communication 10 ECTS Semester 1 7-point grading scale
  1st year exam
  Exam details See courses

Experimental methods 2
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- How the general linear model (GLM) is used for the analysis of brain scan data
- Basic assumptions about multivariate statistics
- Various types of multivariate analyses, such as principal component analysis, canonical correlation and linear discriminant analysis

Skills:
After completing the course, students will be able to:

- Understand, develop and carry out complex experiments
- Assist in analyses of brain scan data that use the general linear model
- Analyse multiple types of data using multivariate statistics

Competences:
After completing the course, students will be able to:

- Explain what multivariate analysis includes and when this kind of analysis is suitable
- Compare univariate and multivariate statistics and identify strengths and weaknesses of the two methods
- Identify the potential for further learning in relation to analyses of brain data using the general linear model
- Identify the potential for further learning in relation to multivariate analyses
  See courses
  Experimental methods 2 10 ECTS Semester 2 Passed /failed
  1st year exam
  Exam details See courses

Introduction to Cognitive Neuroscience
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- The structure and function of the neuron
- The overall structure of the brain
- The primary neural systems involved in cognitive processes
- Research methods pertaining to functional cognitive neuroscience
- Practical data collection using neuroscientific methods

Skills:
After completing the course, students will be able to:

- Critically evaluate published research in the field of cognitive neuroscience
- Critically evaluate experimental designs to be used in cognitive neuroscience
- Assist in the implementation of neuroscientific experiments

Competences:
After completing the course, students will be able to:

- Evaluate strengths and weaknesses of different methods within the field of cognitive neuroscience
- Assume an independent role in cross-disciplinary projects regarding neuroscientific experiments with a cognitive aim
  See courses
  Introduction to Cognitive Neuroscience 10 ECTS Semester 2 7-point grading scale
  Exam details See courses

Studium Generale
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- The intellectual origin of cognitive science
- How cognitive science has been shaped throughout its history
- The central scientific paradigms embedded in cognitive science, and how these affect the research carried out in the field
- The social and ethical aspects of cognitive research

Skills:
After completing the course, students will be able to:

- Compare and contrast cognitive scientific arguments in relation to their truth value, deductive validity and inductive strength
- Contextualise chains of reasoning within the field of cognitive science
- Identify elements of research design connected to issues of research ethics

Competences:
After completing the course, students will be able to:

- Carry out philosophical analyses of theories and methods from the field of cognitive science
- Place experiments and theories used within the field of cognitive science for the purposes of research dissemination and reporting
  See courses
  Studium Generale 10 ECTS Semester 2 7-point grading scale
  Exam details See courses

Experimental methods 3
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- The conceptual assumptions underlying linear and non-linear statistical methods and the implications of choosing between such methods
- Strengths and weaknesses of non-linear methods such as Recurrence Quantification Analysis and Bayesian statistics

Skills:
After completing the course, students will be able to:

- Motivate the choice between linear and non-linear statistical methods of analysis
- Design empirical studies with particular consideration of the strengths and weaknesses associated with the statistical methods used
- Carry out non-linear data analyses

Competences:
After completing the course, students will be able to:

- Critically choose between, compare and substantiate the best qualified statistical tools for a given data set and research question
- Explain the results of linear and non-linear statistical analyses and their relevance to the research question
  See courses
  Experimental methods 3 10 ECTS Semester 3 Passed /failed
  Exam details See courses

Mind and consciousness
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- The most important scientific paradigms relating to research within the field of cognitive science
- The theoretical and philosophical basis of cognitive science, with special emphasis on research into the consciousness
- Implicit a priori models and assumptions underlying empirical research, including applied research into cognition and consciousness (for example in a health and consumer context)

Skills:
After completing the course, students will be able to:

- Distinguish between a priori and empirical issues within cognitive science
- Identify the most important philosophical questions and assumptions within dominant scientific paradigms
- Critically assess empirical research on the consciousness in relation to fundamental questions

Competences:
After completing the course, students will be able to:

- Evaluate research on cognitive science with regard to fundamental theoretical themes of the course
- Critically discuss research on cognitive science in relation to the most important scientific paradigms
- Analyse the role that implicit models and assumptions play in the design and interpretation of applied research on cognitive science
  See courses
  Mind and consciousness 10 ECTS Semester 3 7-point grading scale
  Exam details See courses

Humanities and International electives (HUM- & IV-fag)
10 ECTS

See courses

Computational Modeling for Cognitive Science
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- The conceptual assumptions underlying computational models in cognitive science
- Strengths and problems associated with the use of computational methods in cognitive science

Skills:
After completing the course, students will be able to:

- Design and program simple computer models of elements of cognitive processes
- Formulate and test statistical hypotheses by means of computer simulation

Competences:
After completing the course, students will be able to:

- Explain the reasons for using various models
- Place modelling in relation to the other academic practice within cognitive science
- Identify their own needs for further learning
  See courses
  Computational modeling for cognitive science 10 ECTS Semester 4 Passed /failed
  Exam details See courses

Models of perception and action
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- Recent quantitative theories about the link between perception and action
- The neural basis for perception and action
- Changes in perception and action processes as the product of learning or cognitive/neurological disabilities
- How perception and action interact with (and are affected by) technology

Skills:
After completing the course, students will be able to:

- Develop and test basic quantitative hypotheses about the relationship between perception and action
- Analyse the design of technology, such as user interfaces and aids for the physically disabled in relation to basic principles of perception and action

Competences:
After completing the course, students will be able to:

- Evaluate, design and interpret basic quantitative research in the fields of perception and action
- Evaluate rehabilitation programmes based on basic principles of perception and action
  See courses
  Models of perception and action 10 ECTS Semester 4 7-point grading scale
  Exam details See courses

Social and cultural dynamics in cognition
10 ECTS

Knowledge:
After completing the course, students will have gained knowledge of:

- The effect of social and cultural processes on cognition

Skills:
After completing the course, students will be able to:

- Develop and critically evaluate hypotheses concerning the effect of social and cultural factors on cognition
- Design simple studies of the effect of socio-cultural factors on cognition

Competences:
After completing the course, students will be able to:

- Critically assess and select methods for the study of interaction between socio-cultural factors and cognition
  See courses
  Social and cultural dynamics in cognition 10 ECTS Semester 4 7-point grading scale
  Exam details See courses

Bachelor project
15 ECTS

Knowledge:
After completing the Bachelor’s project, students will have gained knowledge of:

- Existing theoretical and empirical research in a topic of their own choice
- The relations between the topic chosen and central paradigms in cognition science
- Methods of investigation of the chosen topic

Skills:
After completing the Bachelor’s project, the student can:

- Independently carry out all phases of a research investigation within cognitive science
- Develop deeper and more specialised knowledge and skills in an area of interest
- Communicate the knowledge acquired clearly and coherently in English

Competences:
After completing the course, students will be able to:

- Demonstrate and structure independent, critical and responsible academic work
- Take a critical and reflective approach to a topic in the field of cognitive science.
- Contribute to empirical, evidence-based knowledge of issues related to human cognition and behaviour
- Identify the need for further knowledge acquisition
  See courses
  Bachelor's project 15 ECTS Semester 5 7-point grading scale
  Exam details See courses

3. General rules
   3.1 Authority
   The Degree Programme Order, Ministerial Order no. 1328 of 15 November 2016 on Bachelor’s and Master’s Degree Programmes at the Universities with subsequent amendments (in Danish only) https://www.retsinformation.dk/Forms/R0710.aspx?id=184781

The Examination Order, Ministerial Order no. 1062 of 30 June 2016 on University Examinations and Grading (in Danish only) https://www.retsinformation.dk/Forms/R0710.aspx?id=183445

Ministerial Order no. 144 of 3 February 2015 on the Grading Scale and Other Forms of Assessment of Study Programmes under the Ministry of Higher Education and Science (Grading Scale Order) with subsequent amendments (in Danish only) (https://www.retsinformation.dk/Forms/R0710.aspx?id=167998).

Ministerial Order no. 107 of 12 February 2018 on Admission to and Enrolment on Bachelor’s Degree Programmes at Universities (in Danish only)(https://www.retsinformation.dk/Forms/R0710.aspx?id=198310).

Aarhus University’s rules regarding degree programmes can be found in the university’s electronic rules and regulations https://international.au.dk/about/organisation/index/

3.2 Regulations for 1st year examination
The university will cancel the enrolment of all students who fail to pass the exams included in their first-year exam by the end of their first year of study. This will be done whether or not students have tried to pass this exam three times. Introductory courses are not included in the first-year exam, and the first year of study begins at the start of the first semester in which there are no non-introductory courses.
The exams included in the first-year exam are stated under point 2 of your academic regulations (“Degree programme structure”).

3.3 Regulations for commencement of studies exam
The compulsory commencement of studies exam will be held before the end of September on the first semester of the Bachelor’s degree programme.
The exam generates 0 ECTS credits and is a pass/fail internal examination with no co-examiner. Two attempts to pass this exam are allowed. First-semester students will be registered for the exam automatically. The result will be posted in the student’s self-service system (mit.au.dk). Students who do not pass their first examination attempt will be registered automatically for their second attempt.
The result of the second attempt will be posted in the student’s self-service system (mit.au.dk) no later than 28 September. Students who fail both their first and second attempt will be withdrawn from their degree programme with immediate effect and without further warning.

3.4 Credit and flexibility
Students may apply for advance approval of degree programme elements which are subsequently completed at another university or institution of higher education in Denmark or abroad. An approved application for advance approval of transfer credit obligates the student to document his or her results – both passed and failed degree programme elements, as soon as these marks are available. The board of studies may – if necessary – procure the necessary documentation directly. The student will then receive transfer credit for the passed degree programme elements.

Applications for advance approval of transfer credit and submission of documentation for completed degree programme elements with a view to registration of transfer credit must take place in accordance with the rules published on the study portal. In addition, the board of studies may approve applications for transfer credit for degree programme elements which have been completed at an institution of higher education in Denmark or abroad.

When the board of studies has approved the credit transfer of a passed course/course element from an institution of higher education in Denmark or abroad, credit must be transferred as as ‘Passed’. If the course element in question is assessed in accordance with the 7-point grading scale at both institutions, the assessment is transferred with the grade given, cf. the Degree Programme Order and the Examination Order.

3.5 Registration and withdrawal
Registration for courses, exams and elective courses
Students will be registered automatically for teaching and exams in the compulsory subjects of their degree programme in the order of semesters prescribed in the academic regulations. Students are responsible for ensuring that they are always registered correctly for courses and exams. Students are responsible for registering for and (if necessary) listing their priorities for elective courses. This must be done by the registration deadline. After the deadline for withdrawal, elective courses become a compulsory part of the student’s degree programme and cannot be changed.

Students are responsible for registering for re-examinations by the set deadline, which can be found on the study portal. Students can withdraw from exams using the self-service system at mit.au.dk, following the procedure described on the study portal. Students can only register for re-examinations if they have already tried to pass the scheduled exam before the re-examination.

Students may not withdraw from exams after the deadline for withdrawal, and the failure to take part in an exam for which they are registered will count as an attempt to pass it. The deadlines for registration and withdrawal are identical and can be found on the study portal. Students may not withdraw from their Master’s thesis.

Students who stick to the prescribed curriculum will be given priority when class places are assigned. After the deadline for registration, class places can only be assigned to students if there is room in the classroom concerned.

Forms of re-examination
Students taking exams in the re-examination period will be examined using the form of re-examination that has been specified.

If only one form of exam has been specified, students can ask to take the re-examination in the scheduled exam period. Any such request must be made before the registration deadline. Students should not expect requests submitted after this time to be considered.

If two forms of exam are specified, the first will be used for the scheduled exam and the second for the re-examination. Students can choose between the specified forms of exam for their Master’s thesis.

Requirement regarding active enrolment
The active enrolment requirement of 45 ECTS per academic year applies to all students, cf. Aarhus University’s rules of 1 September 2018: http://www.au.dk/om/organisation/regelsamling/4/41/ , which means that students must pass at least 45 ECTS credits each academic year.

3.6 Spelling and fluency
In the assessment of Bachelor’s projects and other major written assignments, emphasis must be placed on the student’s spelling and fluency in addition to his/her command of the content knowledge.

In the assessment of all written exams, emphasis will be placed on the extent to which the student is able to present an academic topic and structure an academic assignment including complying with formal academic requirements (references, quotations, etc.). In the assessment of all oral exams, emphasis is placed on the student's ability to present academic information, structure a presentation and take part in constructive academic dialogue.

3.7 Regulations for assignments
Conditions regarding the extent of written submissions are stated in the description of each degree programme component. A normal page for written submissions is 2400 characters (with spaces). To calculate normal pages, both text and notes are included, but not the front page, table of contents and bibliography. Written submissions that do not comply with these conditions cannot be accepted for assessment.

3.8 Using computers for examinations
Most written exams at AU are taken in a digital exams system. More information about digital exams may be found on the study portal. See also Aarhus University’s rules on on-site examinations.

http://www.au.dk/en/about/organisation/index/5/56/56-06-rules-on-on-site-examinations-aus-rules/

3.9 Project-oriented courses
Options for project-oriented courses are stated in the description of the individual study element.
3.10 Exemption
An exemption is a deviation from that or those regulations that normally apply for the area in question. Exemption can be granted on the basis of an application sent to the authority that has the power to grant such exemption. An application for exemption must be submitted to the Board of Studies. If another authority has the power to grant exemption, the Board of Studies forwards the application to the appro-priate authority (e.g. the dean, rector or ministry). An application for exemption must be made in writing, stating reasons, and submitted as soon as possible. For the application to be processed immediately, it must include a precise account of the regulation from which exemption is sought, and what such exemption is intended to achieve (e.g. permission to use special aids, extension of examination time, postponement of time limits). Documentation for the unusual conditions that justify exemption must be enclosed with the application. Importance will not normally be attached to such conditions if they are not documented.More information about exemptions may be found on the study portal portal.

3.11 Appeals and complaints
Complaints about exams should be submitted to the university. For the application to be processed, it must be made in writing and must account for the student’s reasons for lodging the complaint. The complaint must state both the reason for the complaint and what the complainant expects to achieve. Complaints about exams must be submitted within fourteen days after the deadline for the publication of exam results, cf. the Examination Order. More information about exam complaints is available on the study portal.
3.12 Examinations
Prerequisites
Prerequisites are demands which students must comply before taking an exam. Such prerequisites normally involve the submission and approval of a syllabus, a media product, a case, a collection of assignments or a synopsis.
The rules and deadlines for complying with prerequisites are stated on the study portal.

3.13 Exchange
A degree programme may include study abroad. More information is available on the study portal.

4. Changes to the academic regulations
   Per 01.09.2016:
   Progression requirements is chancelled
   Update of general rules
   Update of "about the degree programme"
   Update of section 1.3

Per 01.09.2017:
Update of general rules

Per 01.09.2018:

- Update of general rules
- Computational Modeling for Cognitive Science: change in the ordinary exam form from "lecture participation" to "portfolio" and the re-examination form from "set assignment + set oral exam" to "portfolio"
- Introduction to Cognitive Neuroscience: change in the re-examination form from "set assignment + set oral exam" to "take-home assignment + set oral exam"

  02.01.2019:
  Text added about the first year exam for students enrolled before 1 September 2016.

  01.02.2019:
  Legal right to be admitted changed from the Master’s degree programme in information studies to the Master's degree programme in Cognitive Science.

  01.09.2019:
  Bachelor's project:﻿ Text on group exams has been updated.
  Added text on commencement of studies exam to the study diagram.
  Introduction to Cognitive Neuroscience: added explanation - the synopsis is prepared on the basis of a dataset prepared by the student (ordinary exam) or handed out by the teacher (re-examination).
  Update of general rules.
