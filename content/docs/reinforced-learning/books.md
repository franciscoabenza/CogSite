---
weight: 6
title: "Books"
---



# Books

{{< details title="Meta Sites" open=false >}}
### Archive - Online Archive of almost anything you could imagine
[link](https://archive.org/)

### Gutenberg - Non-profit digital library offering millions of free public domain books (Almost all the classics) 
[link](http://www.gutenberg.org/)

### LibriVox - Non-profit digital volunteer-run initiative that aims to release public domain audiobooks. 
{{< /details >}}

{{< details title="Meta Books" open=false >}}
### R for data scientists - This book will teach you how to do data science with R
[link](https://r4ds.had.co.nz/)

### A Language, not a Letter: Learning Statistics in R 
[link](https://ademos.people.uic.edu/index.html)
{{< /details >}}

{{< details title="Teacher Recomendations" open=false >}}
## Savhannah Schulz
### The Predictive Mind by Jakob Hohwy
[link](https://www.goodreads.com/book/show/18012311-the-predictive-mind)

A wonderful introduction to the world of Predictive Processing in an easily digestible format. If you look closer at the preface, you will also find familiar names and realise that Hohwy was a lecturer at AU in the early two-thousands.

### METACOGNITIVE DIVERSITY by Joëlle Proust
[link](https://www.goodreads.com/book/show/38139596-metacognitive-diversity)

If you are interested in meta-cognition, this book presents an insightful overview over different approaches to it from an interdisciplinary angle (e.g. religion studies, developmental psychology).

### The Human Brain Colouring Book
[link](https://www.saxo.com/dk/the-human-brain-coloring-book_diamond-books_paperback_9780064603065?gclid=CjwKCAjw7-P1BRA2EiwAXoPWA0UP0oKyx6DAT-JM2Vb0mI7V-E4qaj5Y5s3Hnn6laa05AuV_MGM-SxoCB5AQAvD_BwE)

If you had enough of reading, colouring in brains and lobes can be surprisingly calming

### Time of the Magicians
[link](https://www.goodreads.com/book/show/51285900-time-of-the-magicians)

This book by Eilenberger is a well researched narrative about four influential thinkers (Wittgenstein, Benjamin, Cassirer and Heidegger) and their lives and philosophies. Interesting background and links. For example, did you know that Heidegger and Hannah Arendt were a couple?

## Riccardo Fusaroli

### The model thinker
[link](https://www.goodreads.com/en/book/show/39088592)

What are models? Why should we absolutely use them, whenever doing serious thinking? Why should we absolutely distrust them and pitch them against each other?

### Artificial Intelligence

[link](https://www.goodreads.com/book/show/43565360-artificial-intelligence)

What is all this fuzz about deep learning? What are its basic concepts? What are the strengths and deep limitations?

### Infinite powers 

[link](https://www.goodreads.com/book/show/40796176-infinite-powers)

Calculus as you have never seen it. A vertiginous trip through the mind-bending discovery of infinitesimals. <br> 

**Extra : Honorable mentions** 

### Pearl, Causal Inference in Statistics - A Primer. 

[link](https://www.goodreads.com/book/show/27164550-causal-inference-in-statistics)

Sometimes a correlation does imply causation. When, how, WTF? A little book with exercises trying to guide you through causal inference issues in a meta-statistical way: causality can be found in statistical models given the assumptions you make about the underlying mechanisms.

### King. Gods of the upper air 
[link](https://www.goodreads.com/book/show/42951225-gods-of-the-upper-air)

### Enfield. How we talk 
[link](https://www.goodreads.com/book/show/34523270-how-we-talk)

### Christakis. Blueprint 
[link](https://www.goodreads.com/book/show/40696923-blueprint)

### Jakson. The human network 
[link](https://www.goodreads.com/book/show/35794820-the-human-network)

### Henrich. The secret of our success 
[link](https://www.goodreads.com/book/show/25761655-the-secret-of-our-success)

## Rebekah Brita Baglini

### Algorithms to Live By (Brian Christian and Tom Griffiths) 
[link](https://www.goodreads.com/book/show/25666050-algorithms-to-live-by)

### The Book of Why (Dana Mackenzie and Judea Pearl) 
[link](https://www.goodreads.com/book/show/36204378-the-book-of-why)

#### The Linguistics Wars (Randy Allen Harris) 
[link](https://www.goodreads.com/book/show/567061.The_Linguistics_Wars) 
{{< /details >}}