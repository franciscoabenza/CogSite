---
weight: 10
# bookFlatSection: true
title: "CogSci Productions"
---

# Cog Productions

## CogTalks

{{< details title="Psychedelics with Lasse D. Hansen" open=false >}}
{{< youtube 2qp2Hltyn4w >}}
{{< /details >}}

{{< details title="Citizen Science with Blanka Palfi" open=false >}}
{{< youtube J6Tw4D3MKYA >}}
{{< /details >}}

{{< details title="Casual Causality with Jonathan Rystrøm" open=false >}}
{{< youtube xMvI4bLWoSU >}}
{{< /details >}}

{{< details title="Brain-Machine Interfacing with Esben Kran" open=false >}}

{{< youtube nDDIAIdYCoc >}}

[Download slides](../../../cogtalks/brain-machine/slides.pdf)

### Notes

#### References

- <https://neuralink.com>
  - Neuralink, the example for invasive brain-machine interfacing technology used in the talk
- <https://www.braingate.org/>
  - The academic consortium using the Utah Array and developing next-generation academic brain-machine interfaces
- <https://www.kernel.co/hello-humanity>
  - Kernel, the main non-invasive brain-machine interfacing technology used as an example in the talk
- <https://www.extremetech.com/extreme/162678-harvard-creates-brain-to-brain-interface-allows-humans-to-control-other-animals-with-thoughts-alone>
  - Study where a human controlled a rat tail through brain signals, brain-to-brain interface
- <https://www.the-scientist.com/daily-news/a-brain-to-brain-interface-for-rats-39712>
  - Two rats with connected brains

{{< /details >}}

{{< details title="Consciousness with Christoffer L. Olesen" open=false >}}
{{< youtube SeXONznYB0s >}}
{{< /details >}}
