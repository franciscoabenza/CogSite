---
weight: 10
# bookFlatSection: true
title: "Brain games"
---

# Brain games
## CogFight
Lasse's fantastic card game corner.

## Personally programmed games
- Esben's [depressed flower](https://esbenkc.itch.io/keep-her-happy)

# Cool games
[Universal Paperclips Game](https://www.decisionproblem.com/paperclips/)
[Can't  Unsee](https://cantunsee.space/?fbclid=IwAR0VxbCsXp61JbetoxMjcN-expzxee0j3-h8Ir7qITpwwjBqq-s85Y8j_nI)

# Initiatives in the Brain Games
## CitizenScience
## ScienceAtHome
