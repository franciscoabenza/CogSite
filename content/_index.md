---
title: "Cognitive Science"
bookFlatSection: true
bookToC: true
---

<!-- ![Cognitive science cover image](../cover.jpg)-->

# What is cogsci.dk?

Cogsci.dk is the website for all information relevant
to Cognitive Science students and interested at Aarhus University
and associated institutions.

## Who are the people behind this?

The development of this website has been organized
by and for students of Cognitive Science at Aarhus
University, Denmark. The website is actively maintained
and updated by the community and the core team <3

## How can I help?

You can do one or more of the following:

- Share this website with your fellow students!
- Write or propose a page to add to the website
- Write a [blog post on CogSite](docs/contribute/writing-a-blog-post)
- Add your [exam project on the CogSite](docs/contribute)
- Propose other cool things to add (can be **anything**) - contact us [here](docs/contact)

Information on writing the documents on this website
can be found in the [Contribute](docs/contribute) section.
Contact the core team [here](docs/contact).

# Section information

## Contact

The [contact](docs/contact/_index.md) section
contains the contact information of all relevant
parties at Aarhus University that you might need contact
information on. It features contact information for people like
[the core team](docs/contact/_index.md),
[teacher](docs/contact/all-teachers.md),
[cogtalks](docs/contact/cogtalks.md),
[neural network](docs/contact/neural-network-newsletter.md),
[Broca's Bodega](docs/contact/kognitionsfaglig-forening.md),
[Gazzaniga's Gang](docs/contact/kognitionsfaglig-forening.md),
and [others](docs/contact/others.md).

## Contribute

The [contribute](docs/contribute/_index.md) section contains
information on how you can help add and update the website.
It has extensive guides on anything from
[writing in markdown](docs/contribute/writing-on-the-cogsite.md)
(the format of the files on this website) to adding
[blog posts](docs/contribute/writing-on-the-cogsite.md).

## The Library

In [The Library](docs/the-library/_index.md), you will find
[documents of the past](docs/the-library/earlier-exam-projects.md)
(like exam projects) and the
[halls of fame](docs/the-library/student-publications.md)
that demand glory in the Cognitive Science program!

## Social System

In the [social system](docs/social-system/_index.md), you will
find the beautiful, loving relationships available on the
Cognitive Science program. You can find the
[social groups](docs/social-system/social-groups.md),
[book market](docs/social-system/book-market.md),
[international support](docs/social-system/international-support.md),
[the study café](docs/social-system/study-cafe.md),
[tutors for help with your understanding](docs/social-system/tutor-list.md),
[brain games](docs/social-system/brain-games.md), and the
[AU environment around CogSci](docs/social-system/au-environment.md).

## Programming and Statistics

In this section, you can find all that which has been missing
in your life. Access to endless
[R cheat sheets](docs/programming-and-statistics/r-cheat-site.md),
incredible [package lists](docs/programming-and-statistics/r-packages.md),
magnificent mathematics, and beautiful statistics. The joy of
logic is upon us.

## Reinforced Learning

In [Reinforced Learning](docs/reinforced-learning/_index.md),
you will find access to lists and lists of glorious resources
from [online courses](docs/reinforced-learning/online-courses.md)
to enticing [books](docs/reinforced-learning/books.md) and
[videos](docs/reinforced-learning/videos.md).

## Formal stuff

We know that Aarhus University's organizational structure can
be a bit daunting which is why we have attempted to gather all
of the best resources _you_ need for your student experience in
the very formally named [Formal Stuff](docs/formal-stuff/_index.md).

## Career

In the [career section](docs/careers/_index.md), there is ample
opportunity to get lost in all of the possibilities that
Cognitive Science brings you. With everything from
[entrepreneurship](docs/careers/the-kitchen.md) to
[alumni stories](docs/careers/alumni.md) and
[internships](docs/careers/job-list.md).
